# Description #

This is a small python program for an esp8266 with micropython deployed onto it. 
This program reads input from an analog pin and then scales that value for the duty cyle output value, which sets the servo position.  

### Live Demonstration 
View a live demo [here](https://bit.ly/2GXYGdO)


