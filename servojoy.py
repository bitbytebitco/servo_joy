import machine

# init analog input (joystick)
adc = machine.ADC(0)

# init servo
servo = machine.PWM(machine.Pin(5), freq=50)

def scale_input(n):
    return ((n-0)/(1024-0))*(115-28)+28

def read_joystick():
    while True:
        n = adc.read()
        mnum = int(scale_input(n))
        servo.duty(mnum)
